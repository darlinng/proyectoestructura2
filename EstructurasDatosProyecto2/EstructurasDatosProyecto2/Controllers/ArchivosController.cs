﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EstructurasDatosProyecto2.Models;
using System.Web.Script.Serialization;

namespace EstructurasDatosProyecto2.Controllers
{
    public class direc
    {
        public int id { get; set; }
        public string text { get; set; }
        public DateTime creacion { get; set; }

    }
    [Authorize]
    public class ArchivosController : Controller
    {
        public static List<DirectoriosModelo> ParaBuscar = new List<DirectoriosModelo>();
    
    estructura2Entities bd = new estructura2Entities();
        public static int IdDirectorioPadre = 1;//el id por defecto del directorio padre sera siempre 1
        public static int DirectorioEliminar = 0;
        // GET: Archivos


        public ActionResult Index()
        {
            //para llenar la lista por primera vez
            init();
            return View();
        }
        //crea un directorio
        public ActionResult _CrearDir()
        {
            var lista = from s in bd.directorios
                        where s.estado == 1
                        select new DirectoriosModelo
                        {
                            id = s.id,
                            nombre = s.nombre
                        };



            ViewBag.listaDirectorios = new SelectList(lista.ToList(), "id", "nombre");
            return PartialView();
        }
        [HttpPost]
        public ActionResult _CrearDir(DirectoriosModelo dir)
        {
            directorio newdir = new directorio();
            newdir.nombre = dir.nombre;
            newdir.padre = IdDirectorioPadre;
            newdir.estado = 1;
            newdir.fecha_creacion = DateTime.Now;
            bd.directorios.Add(newdir);
            if (bd.SaveChanges() > 0)
            {
                return PartialView("Exito");
            }
            return View();
        }
        public ActionResult Exito()
        {
            return View();
        }
        public ActionResult _ListarDirectorios()
        {
            return PartialView();
        }

             //este metodo es temporal ya que solo guarda el id del directorio padre

        public void temp(int id)
        {
            IdDirectorioPadre = id;

        }
        public void combotem(int id)
        {
            DirectorioEliminar = id;

        }
        //metodo de ayuda para generar los hijos de los directorios aun no funciona
        public int[] child(int itm)
        {
            var dato = from s in bd.directorios
                       where s.padre == itm
                       select new DirectoriosModelo
                       {
                           id = s.id,
                           
                       };
            dato.ToList();
            int[] listahijos = new int[dato.Count()];
            int i = 0;
            foreach (var it in dato)
            {
                listahijos[i]=it.id;
                i++;
            }
                 

            return listahijos;
        }

        //desde la vista se llama este metodo para generar un json 
        public JsonResult llamarjson()
        {
            //En realidad solo estamos ocupando esta linea para obtener la lista de directorios
            var salida = llenarDirectorios();
            List<direc> objeto = new List<direc>();
            //int[] lista = new int[salida.Count];

            //int i = 0;
            //foreach (var item in salida)
            //{
            //    lista = child(item.id);
            //    if (lista.Length>0)
            //    {
            //        for (int j=0;j<lista.Length;j++)
            //        {
            //            if (lista[j]!=0)
            //            {
            //                var ob = new dir
            //                {
            //                    id = item.id,
            //                    text = item.nombre,
            //                    child = new children
            //                    {
            //                        id = lista[j],
            //                        text=BuscarNombre(lista[j])
            //                    }
            //                };
            //                objeto.Add(ob);                       }
            //            else
            //            {
            //                continue;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        var ob = new dir
            //        {
            //            id = item.id,
            //            text = item.nombre
            //        };
            //        objeto.Add(ob);
            //    }

            //}

            //este es el formato que se busca para el json
            // [{"id":1,"text":"Root node","children":[{"id":2,"text":"Child node 1"},{"id":3,"text":"Child node 2"}]}]

            foreach (var item in salida)
            {
                objeto.Add(new direc() {id=item.id,text=item.nombre,creacion=item.fecha_creacion.Value});
            }

            return Json(objeto, JsonRequestBehavior.AllowGet);
        }

        //llena la lista de directorios
        private List<DirectoriosModelo> llenarDirectorios()
        {
            var it = from s in bd.directorios
                     where s.estado == 1
                     select new DirectoriosModelo
                     {
                         id = s.id,
                         nombre = s.nombre,
                         padre = s.padre,
                         estado = s.estado,
                         fecha_creacion = s.fecha_creacion

                     };
            return it.ToList();
        }

        //Este metodo es temporal, busca llenar una lista para no estarla pidiendo a la base de datos
        //se debe buscar una opcion mejor
        public void init()
        {
            if (ParaBuscar.Count()==0)
            {
                var it = from s in bd.directorios
                         where s.estado == 1
                         select new DirectoriosModelo
                         {
                             id = s.id,
                             nombre = s.nombre,
                             padre = s.padre,
                             estado = s.estado,
                             fecha_creacion = s.fecha_creacion

                         };
                it.ToList();
                foreach (var i in it)
                {
                    ParaBuscar.Add(new DirectoriosModelo() { id = i.id, nombre = i.nombre, padre = i.padre });
                }
            }        

        }


        public ActionResult _solicitudArchivo(string idDatos)
        {
            int id = 1;//por defecto mostrara los archivos de la raiz
            try
            {
                id = Convert.ToInt32(idDatos);
            }
            catch (Exception)
            {
               
            }

            var lista = from s in bd.archivoes
                        where s.id_directorio==id
                        select new ArchivoModelo
                        {
                            id = s.id,
                            nombre = s.nombre,
                            tipo = s.tipo,
                            fecha_creacion = s.fecha_creacion,
                            id_directorio = s.id_directorio,
                            datos = s.datos
                        };
       
            return PartialView(lista.ToList());
        }

        public string BuscarNombre(int id)
        {
            foreach (var item in ParaBuscar)
            {
                if (item.id.Equals(id))
                {
                    return item.nombre;
                }
            }

            return null;
        }

        [HttpPost]
        public ActionResult _EliminarDirectorio(DirectoriosModelo dir)
        {
      
            return View();
        }

        public ActionResult _EliminarDirectorio()
        {
            List<DirectoriosModelo> disponibles = new List<DirectoriosModelo>();
            
            var lista = from s in bd.directorios
                        select new DirectoriosModelo
                        {
                            id = s.id,
                            nombre = s.nombre,
                            padre = s.padre,
                            estado = s.estado,
                            fecha_creacion = s.fecha_creacion

                        };
            lista.ToList();
            //esta parte solo retorna los directorios que no tienen hijos
            int[] padres = new int[lista.Count()+1];
            int i = 0;
            foreach (var item in lista)
            {
                padres[i] = item.padre.Value;
                i++;
            }
            int bande = 0;
            foreach (var it in lista)
            {
                bande = 0;
                for (int l=0;l<padres.Length;l++)
                {
                    
                    if (it.id.Equals(padres[l]))
                    {
                        bande++;
                    }
                }
                if (bande==0)
                {
                    disponibles.Add(new DirectoriosModelo() { id = it.id, nombre = it.nombre });

                }
                //int band = 0;
                //for (int k=0;k<padres.Length;k++)
                //{
                //    if (it.padre.Value == padres[k])
                //    {
                //        band++;
                //    }
                //}
                //if (band == 0)
                //{
                //    disponibles.Add(new DirectoriosModelo() { id = it.id, nombre = it.nombre });
                //}

            }

            ViewBag.listaDir = new SelectList(disponibles, "id", "nombre");
            return PartialView();
        }

        public ActionResult EliminarCarpeta()
        {
            List<DirectoriosModelo> disponibles = new List<DirectoriosModelo>();

            var lista = from s in bd.directorios
                        where s.estado==1
                        select new DirectoriosModelo
                        {
                            id = s.id,
                            nombre = s.nombre,
                            padre = s.padre,
                            estado = s.estado,
                            fecha_creacion = s.fecha_creacion

                        };
            lista.ToList();
            //esta parte solo retorna los directorios que no tienen hijos
            int[] padres = new int[lista.Count() + 1];
            int i = 0;
            foreach (var item in lista)
            {
                padres[i] = item.padre.Value;
                i++;
            }
            int bande = 0;
            foreach (var it in lista)
            {
                bande = 0;
                for (int l = 0; l < padres.Length; l++)
                {

                    if (it.id.Equals(padres[l]))
                    {
                        bande++;
                    }
                }
                if (bande == 0)
                {
                    disponibles.Add(new DirectoriosModelo() { id = it.id, nombre = it.nombre });

                }
                //int band = 0;
                //for (int k=0;k<padres.Length;k++)
                //{
                //    if (it.padre.Value == padres[k])
                //    {
                //        band++;
                //    }
                //}
                //if (band == 0)
                //{
                //    disponibles.Add(new DirectoriosModelo() { id = it.id, nombre = it.nombre });
                //}

            }

            ViewBag.listacarp = new SelectList(disponibles, "id", "nombre");

            return View();
        }
        public ActionResult deleteDir(string id)
        {
            int dir=0;
            try
            {
                 dir = Convert.ToInt32(id);
            }
            catch (Exception)
            {

               
            }
            var result = bd.directorios.SingleOrDefault(b => b.id == dir);
            if (result != null)
            {
                result.estado = 2;
                if (bd.SaveChanges()>0) {
                  return View(ViewBag.resultado = "La Carpeta se elimino con exito, Recargue la pagina para que vea los resultados");
                }
                
            }

            return View();
        }
    }

}