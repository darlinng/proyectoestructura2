﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EstructurasDatosProyecto2.Controllers
{

  

    public class children
    {
        public int id { get; set; }
        public string text { get; set; }

    }

    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            if (!User.Identity.IsAuthenticated) {

                ViewBag.autenticado = "NO";
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

     
    }
}