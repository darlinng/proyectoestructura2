﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstructurasDatosProyecto2.Models
{
    public class ArchivoModelo
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string tipo { get; set; }
        public Nullable<System.DateTime> fecha_creacion { get; set; }
        public Nullable<int> id_directorio { get; set; }
        public byte[] datos { get; set; }
    }
}