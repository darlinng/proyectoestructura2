﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstructurasDatosProyecto2.Models
{
    public class DirectoriosModelo
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public Nullable<int> padre { get; set; }
        public Nullable<int> estado { get; set; }
        public Nullable<System.DateTime> fecha_creacion { get; set; }
    }
}