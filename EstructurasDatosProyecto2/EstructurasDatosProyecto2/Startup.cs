﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EstructurasDatosProyecto2.Startup))]
namespace EstructurasDatosProyecto2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
